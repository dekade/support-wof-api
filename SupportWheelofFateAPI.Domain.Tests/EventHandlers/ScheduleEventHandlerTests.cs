﻿using System;
using System.Threading.Tasks;
using Moq;
using SupportWheelofFateAPI.Domain.EventHandlers;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Repositories;
using SupportWheelofFateAPI.Domain.Services;
using Xunit;

namespace SupportWheelofFateAPI.Domain.Tests.EventHandlers
{
    public class ScheduleEventHandlerTests
    {
	    private readonly Mock<IScheduleRepository> _mRepository;
	    private readonly Mock<IEngineersService> _mEngineersManager;
	    private readonly ScheduleEventHandler _tEventHandler;
	    private readonly BookedEvent _dBookedEvent;
	    private readonly UnassignedEvent _dUnassignedEvent;


	    public ScheduleEventHandlerTests()
	    {
		    _mRepository = new Mock<IScheduleRepository>();
		    _mEngineersManager = new Mock<IEngineersService>();
		    _tEventHandler = new ScheduleEventHandler(_mRepository.Object, _mEngineersManager.Object);
		    _dBookedEvent = new BookedEvent { Id = Guid.NewGuid(), ScheduleId = "Test"};
			_dUnassignedEvent = new UnassignedEvent { Id = Guid.NewGuid(), ScheduleId = "Test" };
		}


	    [Fact]
	    public void Object_NormalValues_ImplementsEventHandlerForBookedEvent()
	    {
		    var handler = new ScheduleEventHandler(_mRepository.Object, _mEngineersManager.Object);

			Assert.IsAssignableFrom<IEventHandler<BookedEvent>>(handler);
	    }

	    [Fact]
	    public void Object_NormalValues_ImplementsEventHandlerForUnassignedEvent()
	    {
		    var handler = new ScheduleEventHandler(_mRepository.Object, _mEngineersManager.Object);

		    Assert.IsAssignableFrom<IEventHandler<UnassignedEvent>>(handler);
	    }

		[Fact]
	    public async Task HandleBookedEvent_NormalValues_ChecksExistingRecord()
	    {
			await _tEventHandler.Handle(_dBookedEvent);

		    _mRepository.Verify(x => x.Exists(It.IsAny<string>()), Times.Once);
		}

	    [Fact] public async Task HandleBookedEvent_NormalValues_ChecksSpecificRecord()
	    {
		    await _tEventHandler.Handle(_dBookedEvent);

		    _mRepository.Verify(x => x.Exists(It.Is<string>(i => i == _dBookedEvent.ScheduleId)), Times.Once);
	    }

	    [Fact]
	    public async Task HandleBookedEvent_NormalValues_SavesRecord()
	    {
		    await _tEventHandler.Handle(_dBookedEvent);

		    _mRepository.Verify(x => x.Save(It.IsAny<string>(), It.IsAny<ScheduleModel>(), It.IsAny<string>()), Times.Once);
	    }

	    [Fact]
	    public async Task HandleUnassignedEvent_NormalValues_ChecksExistingRecord()
	    {
		    await _tEventHandler.Handle(_dUnassignedEvent);

		    _mRepository.Verify(x => x.Exists(It.IsAny<string>()), Times.Once);
	    }

	    [Fact]
	    public async Task HandleUnassignedEvent_NormalValues_ChecksSpecificRecord()
	    {
		    await _tEventHandler.Handle(_dUnassignedEvent);

		    _mRepository.Verify(x => x.Exists(It.Is<string>(i => i == _dBookedEvent.ScheduleId)), Times.Once);
	    }

	    [Fact]
	    public async Task HandleUnassignedEvent_ModelNotFound_DoesNotSave()
	    {
		    await _tEventHandler.Handle(_dUnassignedEvent);

		    _mRepository.Verify(x => x.Save(It.IsAny<string>(), It.IsAny<ScheduleModel>(), It.IsAny<string>()), Times.Never);
	    }

	    [Fact]
	    public async Task HandleUnassignedEvent_ModelFound_Saves()
	    {
		    var mRepository = new Mock<IScheduleRepository>();
		    mRepository.Setup(x => x.Exists(It.IsAny<string>())).ReturnsAsync(true);
		    mRepository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new ScheduleModel());
			var tEventHandler = new ScheduleEventHandler(mRepository.Object, _mEngineersManager.Object);

			await tEventHandler.Handle(_dUnassignedEvent);
			
		    mRepository.Verify(x => x.Save(It.IsAny<string>(), It.IsAny<ScheduleModel>(), It.IsAny<string>()), Times.Once);
	    }
	}
}
