﻿using System;

namespace SupportWheelofFateAPI.Domain.Models
{
	public class SupportDayModel
	{
		public DateTime CalendarDay { get; set; }
		public EngineerModel AmAssignment { get; set; }
		public EngineerModel PmAssignment { get; set; }

		public bool IsBooked => AmAssignment != null && PmAssignment != null;
	}
}