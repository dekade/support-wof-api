﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Repositories;
using SupportWheelofFateAPI.Domain.Services;

namespace SupportWheelofFateAPI.Domain.EventHandlers
{
	public class ScheduleEventHandler : IEventHandler<BookedEvent>, IEventHandler<UnassignedEvent>
	{
		private readonly IScheduleRepository _repository;
		private readonly IList<EngineerModel> _engineers;

		public ScheduleEventHandler(IScheduleRepository repository, IEngineersService engineersManager)
		{
			_repository = repository;
			_engineers = engineersManager.GetEngineers();
		}

		public async Task Handle(BookedEvent @event)
		{
			var lockKey = @event.ScheduleId;
			var model = new ScheduleModel {Id = @event.ScheduleId, ScheduledDayModels = new List<SupportDayModel>()};

			if (await _repository.Exists(@event.ScheduleId))
				model = await _repository.Get(@event.ScheduleId, lockKey);

			var engineer = _engineers?.FirstOrDefault(e => e.Id == @event.EngineerId);

			var existingDay = model.ScheduledDayModels.FirstOrDefault(d => d.CalendarDay.Date == @event.Date.Date);

			if (existingDay != null)
			{
				if (@event.TimeOfDay == TimeOfDay.Am)
					existingDay.AmAssignment = engineer;
				else
					existingDay.PmAssignment = engineer;
			}
			else
			{
				var bookedDay = new SupportDayModel {CalendarDay = @event.Date};
				if (@event.TimeOfDay == TimeOfDay.Am)
					bookedDay.AmAssignment = engineer;
				else
					bookedDay.PmAssignment = engineer;
				model.ScheduledDayModels.Add(bookedDay);
			}

			await _repository.Save(@event.ScheduleId, model, lockKey);
		}

		public async Task Handle(UnassignedEvent @event)
		{
			var lockKey = @event.ScheduleId;

			if (!await _repository.Exists(@event.ScheduleId)) return;

			var model = await _repository.Get(@event.ScheduleId, lockKey);

			var existingDay = model.ScheduledDayModels?.FirstOrDefault(d => d.CalendarDay.Date == @event.Date.Date);

			if (existingDay != null)
			{
				if (@event.TimeOfDay == TimeOfDay.Am && @event.EngineerId == existingDay.AmAssignment?.Id)
					existingDay.AmAssignment = null;
				if (@event.TimeOfDay == TimeOfDay.Pm && @event.EngineerId == existingDay.PmAssignment?.Id)
					existingDay.PmAssignment = null;
			}

			await _repository.Save(@event.ScheduleId, model, lockKey);
		}
	}
}