﻿using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Events;

namespace SupportWheelofFateAPI.Domain.EventHandlers
{
	public interface IEventHandler<T> where T : IEvent
	{
		Task Handle(T @event);
	}
}
