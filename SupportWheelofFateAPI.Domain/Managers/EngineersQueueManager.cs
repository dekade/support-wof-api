﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupportWheelofFateAPI.Domain.Models;
using SupportWheelofFateAPI.Domain.Services;

namespace SupportWheelofFateAPI.Domain.Managers
{
	public class EngineersQueueManager : IEngineersQueueManager
	{
		private readonly IList<EngineerModel> _engineers;
		private Guid?[] _lastDayEngineerIds;

		private Queue<EngineerModel> AmQueue { get; set; }
		private Queue<EngineerModel> PmQueue { get; set; }

		public EngineersQueueManager(IEngineersService engineersService)
		{
			_engineers = engineersService.GetEngineers();
			Setup();
		}

		private void Setup()
		{
			_engineers.Shuffle();

			AmQueue = new Queue<EngineerModel>(_engineers);

			while (_lastDayEngineerIds != null && _lastDayEngineerIds.Contains(AmQueue.Peek().Id))
			{
				AmQueue.NextInRound();
			}

			PmQueue = new Queue<EngineerModel>(AmQueue);

			var random = new Random();
			var shiftSelector = random.Next(2, _engineers.Count);

			for (var i = 0; i <= shiftSelector; i++)
			{
				var last = PmQueue.Dequeue();
				PmQueue.Enqueue(last);
			}
		}

		public void SetLastDayEngineers(Guid?[] lastDayEngineerIds)
		{
			_lastDayEngineerIds = lastDayEngineerIds;
		}

		public EngineerModel NextAm()
		{
			return AmQueue.NextInRound();
		}

		public EngineerModel NextPm()
		{
			return PmQueue.NextInRound();
		}
	}
}