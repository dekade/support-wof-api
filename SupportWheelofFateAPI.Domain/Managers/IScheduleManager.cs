using System;
using System.Collections.Generic;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Managers
{
	public interface IScheduleManager
	{
		ScheduleModel GenerateMonthPlan(Guid?[] lastDayEngineerIds, int thisYear, int thisMonth);
		IEnumerable<BookedEvent> GetBookedEvents(Guid?[] lastDayEngineerIds, int thisYear, int thisMonth);
	}
}