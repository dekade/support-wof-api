﻿using System;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Managers
{
	public interface IEngineersQueueManager
	{
		void SetLastDayEngineers(Guid?[] lastDayEngineerIds);

		EngineerModel NextAm();
		EngineerModel NextPm();
	}
}