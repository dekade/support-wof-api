﻿using System;
using System.Threading.Tasks;

namespace SupportWheelofFateAPI.Domain.Repositories
{
	public interface IBaseRepository<T>
	{
		Task<T> Get(Guid id, string token = null);

		Task Save(Guid id, T model, string token = null);

		Task<bool> Exists(Guid userId);
	}
}