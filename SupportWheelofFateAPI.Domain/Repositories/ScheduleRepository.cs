﻿using System;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Domain.Repositories
{
	public class ScheduleRepository : BaseRepository, IScheduleRepository
	{
		public ScheduleRepository(RedisSettings settings) : base(settings, "Schedule")
		{
		}

		public async Task<ScheduleModel> Get(string scheduleId, string token = null)
		{
			if (token == null)
				return await base.Get<ScheduleModel>(scheduleId);
			
			var lockAchived = await TryTakeLock(scheduleId, token, DateTime.Now.AddSeconds(3));

			if(!lockAchived)
				throw new TimeoutException($"Unable to get lock for ScheduleModel for id: {scheduleId}");

			return await base.Get<ScheduleModel>(scheduleId);
		}

		public async Task Save(string scheduleId, ScheduleModel model, string token = null)
		{
			await base.Save(scheduleId, model);

			if(token != null)
				await base.Release(scheduleId, token);
		}

		public string MakeKey(int month, int year)
		{
			return $"{year}.{month}";
		}
	}
}