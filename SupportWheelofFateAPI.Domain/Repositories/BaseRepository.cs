﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace SupportWheelofFateAPI.Domain.Repositories
{
	public class BaseRepository
	{
		private readonly RedisSettings _settings;
		private readonly string _redisNamespace;

		public BaseRepository(RedisSettings settings, string redisNamespace)
		{
			this._settings = settings;
			this._redisNamespace = redisNamespace;
		}

		public async Task<bool> Exists(Guid id)
		{
			return await Exists(id.ToString());
		}

		public async Task<bool> Exists(string id)
		{
			var key = MakeKey(id);
			var database = _settings.Multiplexer.GetDatabase();
			var result = await database.StringGetAsync(key);

			return !String.IsNullOrEmpty(result);
		}

		public async Task<T> Get<T>(Guid id)
		{
			return await Get<T>(id.ToString());
		}

		public async Task<T> Get<T>(string id)
		{
			var key = MakeKey(id);
			var database = _settings.Multiplexer.GetDatabase();
			var result = await database.StringGetAsync(key);

			if (String.IsNullOrEmpty(result))
				return default(T);

			return JsonConvert.DeserializeObject<T>(result);
		}

		public async Task Save(Guid id, object objectEntity, DateTime? expiresOn = null)
		{
			await Save(id.ToString(), objectEntity, expiresOn);
		}

		public async Task Save(string id, object objectEntity, DateTime? expiresOn = null)
		{
			var key = MakeKey(id);
			var database = _settings.Multiplexer.GetDatabase();
			if(expiresOn.HasValue)
				await database.StringSetAsync(key, JsonConvert.SerializeObject(objectEntity), (TimeSpan?) (expiresOn - DateTime.Now));
			else
				await database.StringSetAsync(key, JsonConvert.SerializeObject(objectEntity));
		}

		public async Task<bool> DeleteAsync(string key, string token)
		{
			var database = _settings.Multiplexer.GetDatabase();
			return await database.KeyDeleteAsync(key);
		}

		protected string MakeKey(Guid id)
		{
			return MakeKey(id.ToString());
		}

		protected string MakeKey(string key)
		{
			return $"{_settings.Environment}:{_redisNamespace}:{key}";
		}

		public async Task<bool> Lock(Guid guid, string token)
		{
			return await Lock(guid.ToString(), token);
		}

		public async Task<bool> Lock(string id, string token)
		{
			var lockName = $"{MakeKey(id)}_lock";
			var database = _settings.Multiplexer.GetDatabase();
			var result = await database.LockTakeAsync(lockName, token, TimeSpan.FromSeconds(2));
			return result;
		}

		public Task<bool> Release(Guid guid, string token)
		{
			var lockName = $"{MakeKey(guid)}_lock";
			var database = _settings.Multiplexer.GetDatabase();
			return database.LockReleaseAsync(lockName, token);
		}

		public Task<bool> Release(string id, string token)
		{
			var lockName = $"{MakeKey(id)}_lock";
			var database = _settings.Multiplexer.GetDatabase();
			return database.LockReleaseAsync(lockName, token);
		}

		protected async Task<bool> TryTakeLock(string @lock, string token, DateTime timeOut)
		{
			var lockAchived = false;

			while (!lockAchived && timeOut > DateTime.Now)
			{
				lockAchived = await Lock(@lock, token);
				if (lockAchived)
					continue;

				await Task.Delay(TimeSpan.FromMilliseconds(500));
			}
			return lockAchived;
		}
	}
}