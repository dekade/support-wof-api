﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.EventHandlers;

namespace SupportWheelofFateAPI.Domain.EventRouting
{
    public class EventRegistar
    {
		private readonly IServiceProvider _serviceLocator;

	    public EventRegistar(IServiceProvider serviceLocator)
	    {
		    _serviceLocator = serviceLocator ?? throw new ArgumentNullException(nameof(serviceLocator));
	    }

	    public void Register(params Type[] typesFromAssemblyContainingMessages)
	    {
		    var registrar = (IHandlerRegistrar) _serviceLocator.GetService(typeof(IHandlerRegistrar));

		    foreach (var typesFromAssemblyContainingMessage in typesFromAssemblyContainingMessages)
		    {
			    var executorsAssembly = typesFromAssemblyContainingMessage.GetTypeInfo().Assembly;
			    var executorTypes = executorsAssembly
				    .GetTypes()
				    .Select(t => new {Type = t, Interfaces = ResolveMessageHandlerInterface(t)})
				    .Where(e => e.Interfaces != null && e.Interfaces.Any());

			    foreach (var executorType in executorTypes)
			    {
				    foreach (var @interface in executorType.Interfaces)
				    {
					    InvokeHandler(@interface, registrar, executorType.Type);
				    }
			    }
		    }
	    }

	    private void InvokeHandler(Type @interface, IHandlerRegistrar registrar, Type executorType)
	    {
		    var commandType = @interface.GetGenericArguments()[0];

		    var registerExecutorMethod = registrar
			    .GetType()
			    .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
			    .Where(mi => mi.Name == "RegisterHandler")
			    .Where(mi => mi.IsGenericMethod)
			    .Where(mi => mi.GetGenericArguments().Length == 1)
			    .Single(mi => mi.GetParameters().Length == 1)
			    .MakeGenericMethod(commandType);

		    Func<object, CancellationToken, Task> func;
		    func = (@event, token) =>
		    {
			    var handler = _serviceLocator.GetService(executorType);

				if (handler == null)
					throw new InvalidDataException(nameof(executorType));

				return (Task) handler.Invoke("Handle", @event);
		    };

		    registerExecutorMethod.Invoke(registrar, new object[] {func});
	    }

	    private static IEnumerable<Type> ResolveMessageHandlerInterface(Type type)
	    {
		    return type
			    .GetInterfaces()
			    .Where(i => i.GetTypeInfo().IsGenericType && i.GetGenericTypeDefinition() == typeof(IEventHandler<>));

	    }
    }
}
