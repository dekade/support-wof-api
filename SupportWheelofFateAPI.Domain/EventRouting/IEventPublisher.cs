﻿using System.Threading;
using System.Threading.Tasks;
using SupportWheelofFateAPI.Domain.Events;


namespace SupportWheelofFateAPI.Domain.EventRouting
{
	public interface IEventPublisher
	{
		Task Publish<T>(T @event, CancellationToken cancellationToken = default(CancellationToken)) where T : class, IEvent;
	}
}