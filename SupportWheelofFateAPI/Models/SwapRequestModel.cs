﻿using System;
using SupportWheelofFateAPI.Domain.Models;

namespace SupportWheelofFateAPI.Models
{
	public class SwapRequestModel
	{
		public DateTime dateFrom { get; set; }
		public DateTime dateTo { get; set; }
		public Guid engineerFromId { get; set; }
		public Guid engineerToId { get; set; }
		public TimeOfDay sourceTimeSlot;
		public TimeOfDay destinationTimeSlot { get; set; }
	}
}