﻿namespace SupportWheelofFateAPI.Models
{
	public class PopulateScheduleRequestModel
	{
		public int year { get; set; }
		public int month { get; set; }
	}
}