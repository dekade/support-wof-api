﻿using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using MongoDB.Bson.Serialization;
using SupportWheelofFateAPI.Domain.EventRouting;
using SupportWheelofFateAPI.Domain.EventStore;
using SupportWheelofFateAPI.Domain.Managers;
using SupportWheelofFateAPI.Domain.Validators;
using SupportWheelofFateAPI.Middleware;
using Swashbuckle.AspNetCore.Swagger;
using SupportWheelofFateAPI.Domain.EventHandlers;
using SupportWheelofFateAPI.Domain.Repositories;
using StackExchange.Redis;
using SupportWheelofFateAPI.Domain.Events;
using SupportWheelofFateAPI.Domain.Services;

namespace SupportWheelofFateAPI
{
	/// <summary>
	/// Application startup
	/// </summary>
    public class Startup
    {
		/// <summary>
		/// Startup constructor
		/// </summary>
		/// <param name="configuration">IConfiguration</param>
		public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

		/// <summary>
		/// Application configuration
		/// </summary>
		public IConfiguration Configuration { get; }

		/// <summary>
		/// This method gets called by the runtime. Use this method to add services to the container. 
		/// </summary>
		/// <param name="services">IServiceCollection</param>
		public void ConfigureServices(IServiceCollection services)
        {
			// General configurations
			var eventStoreSettings = new EventStore.Settings();
	        Configuration.GetSection("MongoEventStoreSettings").Bind(eventStoreSettings);
	        var readModelSettings = new RedisSettings();
	        Configuration.GetSection("RedisReadModelSettings").Bind(readModelSettings);

	        // Event store configurations
	        services.AddSingleton<EventStore.Settings>(provider => eventStoreSettings);
	        services.AddSingleton<IEventStore, EventStore>();

	        // Event store routing
	        var router = new Router();
	        services.AddSingleton<IEventPublisher>(x => router);
	        services.AddSingleton<IHandlerRegistrar>(x => router);

	        // Repositories
	        services.AddSingleton<IScheduleRepository, ScheduleRepository>();

	        // StackExchange.Redis
	        var mutiplexer = ConnectionMultiplexer.Connect(readModelSettings.ConnectionString);
	        services.AddSingleton<IConnectionMultiplexer>(provider => mutiplexer);
	        readModelSettings.Multiplexer = mutiplexer;
	        services.AddSingleton<RedisSettings>(x => readModelSettings);

	        // Event Handlers
	        // Scan for commandhandlers and eventhandlers
	        services.Scan(scan => scan
		        .FromAssemblies(typeof(ScheduleEventHandler).GetTypeInfo().Assembly)
		        .AddClasses(classes => classes.Where(x => {
			        var allInterfaces = x.GetInterfaces();
			        return
				        allInterfaces.Any(y =>
					        y.GetTypeInfo().IsGenericType && y.GetTypeInfo().GetGenericTypeDefinition() == typeof(IEventHandler<>));
		        }))
		        .AsSelf()
		        .WithTransientLifetime()
	        );
	    
			services.AddApiVersioning(o =>
	        {
		        o.ReportApiVersions = true;
		        o.ApiVersionReader = new HeaderApiVersionReader("api-version");
	        });

			//Web api configuration
	        services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VV");
	        services.AddMvc();
	        services.AddCors(options =>
	        {
		        options.AddPolicy("AllowAllOrigins", builder =>
		        {
			        builder.AllowAnyOrigin();
			        builder.AllowAnyHeader();
			        builder.AllowAnyMethod();
			        builder.AllowCredentials();
		        });
	        });

			
	        services.AddMvc()
		        .AddJsonOptions(
			        options => options.SerializerSettings.ReferenceLoopHandling =
				        Newtonsoft.Json.ReferenceLoopHandling.Ignore
		        );

	        BsonClassMap.RegisterClassMap<BookedEvent>();
	        BsonClassMap.RegisterClassMap<UnassignedEvent>();

			// Register injections
			services.AddSingleton<IEngineersService, EngineersService>();
	        services.AddSingleton<IEngineersQueueManager, EngineersQueueManager>();
			services.AddSingleton<IScheduleValidator, ScheduleValidator>();
	        services.AddSingleton<IScheduleManager, ScheduleManager>();
	        services.AddSingleton<IAllocationSwapManager, AllocationSwapManager>();

			//Register routes
			var serviceProvider = services.BuildServiceProvider();
	        var registrar = new EventRegistar(serviceProvider);
	        registrar.Register(typeof(ScheduleEventHandler));


			// Swagger
			services.AddSwaggerGen(
		        options =>
		        {
			        var provider = services.BuildServiceProvider()
				        .GetRequiredService<IApiVersionDescriptionProvider>();

			        foreach (var apiVersion in provider.ApiVersionDescriptions)
			        {
				        options.SwaggerDoc(
					        apiVersion.GroupName,
					        new Info()
					        {
						        Title = $"Support Wheel of Fate API{apiVersion.ApiVersion}",
						        Version = apiVersion.ApiVersion.ToString()
					        });

				        options.DescribeAllEnumsAsStrings();
			        }
			        var basePath = PlatformServices.Default.Application.ApplicationBasePath;
			        options.IncludeXmlComments(Path.Combine(basePath, "SupportWheelofFateAPI.xml"));
		        });
		}


		/// <summary>
		/// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		/// </summary>
		/// <param name="app">IApplicationBuilder</param>
		/// <param name="env">IHostingEnvironment</param>
		/// <param name="provider">IApiVersionDescriptionProvider</param>
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
		{
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

	        app.UseMiddleware<HealthCheckMiddleware>();
	        app.UseCors("AllowAllOrigins");
	        app.UseDeveloperExceptionPage();
	        app.UseStaticFiles();

			app.UseMvc();

	        app.UseSwagger();
	        var virtualDirectory = env.IsDevelopment() ? "" : "/api/supportwheeloffate";
	        app.UseSwaggerUI(swagOpts =>
	        {
		        foreach (var description in provider.ApiVersionDescriptions)
		        {
			        swagOpts.SwaggerEndpoint(
				        $"{virtualDirectory}/swagger/{description.GroupName}/swagger.json",
				        description.GroupName.ToUpperInvariant());
		        }
	        });
		}
    }
}
